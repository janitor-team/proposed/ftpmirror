BEGIN {
	$| = 1; print("1..3\n");
	$tmpdir = './t';
}
END {
	print("not ok 1\n") unless $loaded;
	rmdirhier($tmpdir) if -d $tmpdir;
}

use Fan::Cool;
use Fan::FTP;
use Fan::Farm;
$Fan::Farm::LOG = 7;

$loaded = 1;
print("ok 1\n");

;# clear first.
rmdirhier($tmpdir);

;# making working directory.
mkdirhier($tmpdir, 0755)
	or print("not ok 2 (mkdirhier)\n"), exit(1);
ref($farm = Fan::Farm->new($tmpdir))
	or print("not ok 2\n"), exit(1);
print("ok 2\n");

# generate index from current directory.
$farm->generate('.') && -f "$tmpdir/index.1"
	or print("not ok 3\n"), exit(1);
print("ok 3\n");

#open(FILE, "$tmpdir/index.1");
#print("opening $tmpdir/index.1... ok\n");
#print while <FILE>;
#close(FILE);

;# skip test 4
print("ok 4\n");

;# clear again.
rmdirhier($tmpdir);

;#
mkdirhier($tmpdir, 0755)
	or print("not ok 5 (mkdirhier)\n"), exit(1);

ref($farm = Fan::Farm->new($tmpdir))
	or print("not ok 5\n"), exit(1);
print("ok 5\n");

;#
$ftp = Fan::FTP->new(
	ftp_server => 'localhost',
	ftp_user => 'anonymous',
	ftp_pass => 'ikuo@intec.co.jp',
);
ref($ftp) && $ftp->login && $ftp->image
	or print("not ok 6\n"), exit(1);
print("ok 6\n");

$farm->synch($ftp, "/db/tmp/index.local")
	or print("not ok 7\n"), exit(1);
print("ok 7\n");

;#
undef $farm;
