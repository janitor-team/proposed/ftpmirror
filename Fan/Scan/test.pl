BEGIN {
	$| = 1; print "1..8\n";
	%tempfiles = ();
}

END {
	print("not ok 1\n") unless $loaded;
	for my $f (keys %tempfiles) {
		warn("unlink($f)...\n"), unlink($f) if -e $f;
	}
}

use Fan::Scan;
$loaded = 1;
print("ok 1\n");

$tmp1 = "tmp$$.1";
$tempfiles{$tmp1}++;
$tmp2 = "tmp$$.2";
$tempfiles{$tmp2}++;

$Fan::Scan::LOG = 6;

$scan = Fan::Scan->new(
	scan_type => 'LOCAL',
	scan_dir => '../.',
);
ref($scan) && $scan->isa('Fan::Scan')
	or print("not ok 2\n"), exit(1);
print("ok 2\n");

$scan->add_filter(\&filter, $tmp1, $tmp2)
	or print("not ok 3\n"), exit(1);
print("ok 3\n");

$fh = Fan::Scan::fileglob($tmp1, 'w');
defined(fileno($fh))
	or print("not ok 4\n"), exit(1);
print("ok 4\n");

while (defined($a = $scan->get)) {
	$a->fill_checksum;
	print $fh $a->to_line."\n";
}
undef $a;
undef $fh; # this cause closing $tmp1.
undef $scan;

$scan =	Fan::Scan->guess('..'),
ref($scan) && $scan->isa('Fan::Scan')
	or print("not ok 5\n"), exit(1);
print("ok 5\n");

$scan->add_filter(\&filter, $tmp1, $tmp2)
	or print("not ok 6\n"), exit(1);
print("ok 6\n");

Fan::Scan::scan_mklist($tmp2, $scan)
	or print("not ok 7\n");
print("ok 7\n");

$f1 = &Fan::Scan::fileglob($tmp1);
defined(fileno($f1)) or die("fileglob: $!");
$f2 = &Fan::Scan::fileglob($tmp2);
defined(fileno($f2)) or die("fileglob: $!");

system 'diff', $tmp1, $tmp2;

while (1) {
	my $a = <$f1>;
	my $b = <$f2>;
	last if !defined($a) && !defined($b);
	defined($a) && defined($b) && !($a cmp $b)
		or print("not ok 8\n"), exit(1);
}
print("ok 8\n");

sub filter {
	my $a = shift;

	if ($a->type eq 'F') {
		$n = $a->name;
		for my $bad (@_) {
			return undef if $bad eq $n;
		}
	}
	1;
}
