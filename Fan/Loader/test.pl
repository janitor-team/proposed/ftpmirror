BEGIN {
	$| = 1;
	print "1..8\n";
	$tmpfile = 'tmp.cf';
}
END {
	print("not ok 1\n") unless $loaded;
	$loader->dumpall if ref($loader) && !$good;
	unlink($tmpfile) if -e $tmpfile;
}

use Fan::Loader;

$loaded = 1;
print("ok 1\n");

# $Fan::Loader::LOG = 7;
# $Fan::Param::LOG = 7;

## start up
%hash_keys = (
	'sysconfdir'			=> 'DIRECTORY',
	'load-config'			=> '',
	'log-mask'			=> '',
	'put-mode'			=> 'BOOLEAN',
	'ftp-server'			=> '',
	'ftp-gateway'			=> '',
	'ftp-timeout'			=> 'INTEGER',
	'ftp-passive'			=> 'BOOLEAN',
	'ftp-user'			=> '',
	'ftp-pass'			=> '',
	'ftp-debug'			=> 'BOOLEAN',
	'ftp-list-method'		=> 's/^(LIST|STAT)$/\U$1/i || undef',
	'unlink'			=>
	'$_ = $_ eq "rename" ? 2 : &Fan::Param::want_boolean($_)',
	'transfer-file-regexp'		=> '',
	'transfer-directory-regexp'	=> '',
	'override-file-regexp'		=> '',
	'override-directory-regexp'	=> '',
	'temp-directory'		=> 'DIRECTORY',
	'remote-directory'		=> '',
	'local-directory'		=> '',
	'remote-db-directory'		=> '',
	'local-db-directory'		=> '',
	'master-db-directory'		=> '',
	'lock-directory'		=> '',
);

%hash_nest = (
	'archive'			=> 'PACKAGE::$_',
	'package'			=> 'PACKAGE::$_',
	'server'			=> 'PACKAGE::$_',
);

open(FILE, ">$tmpfile") or print("not ok 2\n"), exit(1);
print FILE <<"EOT";
package = utils$$
 ftp-server = ftp$$.intec.co.jp
 remote-directory = /pub/utils$$
 local-directory = /pub/utils$$
EOT
close(FILE);

$loader = Fan::Loader->new(
	loader_keys => \%hash_keys,
	loader_nest => \%hash_nest
);
ref($loader) && $loader->isa('Fan::Loader')
	or print("not ok 2\n"), exit(1);
print("ok 2\n");

## from hash
%init_hash = (
	'sysconfdir'		=> '../..',
	'load-config'		=> 'ftpmirror.cf-sample',
	'temp-directory'	=> '/tmp',
);
$loader->combine_hash(\%init_hash, 'INIT')
	or print("not ok 3\n"), exit(1);
print("ok 3\n");

## from options
@lines = (
	"--log-mask=main=7",
	"--load-config+=$tmpfile",
	"--ftp-passive=yes"
);
$loader->parse_option(\@lines, 'OPTION')
	or print("not ok 4\n"), exit(1);
print("ok 4\n");

## check result
$param = $loader->search('OPTION');
ref($param) && $param->isa('Fan::Param')
	or print("not ok 5\n"), exit(1);
print("ok 5\n");

## get value
$sysconfdir = $loader->get_value('sysconfdir', 'INIT', 'OPTION');
$sysconfdir ne '' && -d $sysconfdir
	or print("not ok 6\n"), exit(1);
print("ok 6\n");

## parsing...
$default = $loader->get_value('load-config', 'INIT', 'OPTION');
for my $file (split(/\n/, $default)) {
	if ($file eq '') {
		print("ignore null string\n"); # ingore
	} else {
		$file = "$sysconfdir/$file" if ! -f $file;
		# print("parsing $file...\n");
		$loader->parse_file($file, 'DEFAULT')
			or print("not ok 7\n"), exit(1);
	}
}
print("ok 7\n");

#
$param = $loader->search("PACKAGE::utils$$");
ref($param) && $param->isa('Fan::Param')
	or print("not ok 8 (search)\n"), exit(1);
$param->getval('ftp-server') eq "ftp$$.intec.co.jp"
	or print("not ok 8 (ftp-server)\n"), exit(1);
$param->getval('remote-directory') eq "/pub/utils$$"
	or print("not ok 8 (remote-directory)\n"), exit(1);
$param->getval('local-directory') eq "/pub/utils$$"
	or print("not ok 8 (local-directory)\n"), exit(1);
print("ok 8\n");

$good = 1;
