BEGIN { $| = 1; print "1..6\n"; }
END { print("not ok 1\n") unless $loaded; }

sub prompt {
	my $prompt = shift;
	local $_;

	print STDERR $prompt;
	defined($_ = <STDIN>) || return undef;
	chomp;
	$_;
}

use Fan::FTP;
$loaded = 1;
print("ok 1\n");

chomp($hostname = `hostname`);
$username = getpwuid($<).'@'.$hostname;

$server = &prompt("try to connect anon-FTP server.\nenter server name: ")
	or print("not ok 2\n"), exit(1);
$ftp = Fan::FTP->new(
	ftp_server => $server,
	ftp_user => 'anonymous',
	ftp_pass => $username );
ref($ftp) && $ftp->isa('Fan::TCP')
	or print("not ok 2\n"), exit(1);
print("ok 2\n");

$ftp->login
	or print("not ok 3\n"), exit(1);
print("ok 3\n");

#$ftp->chdir("/tmp/temp")
#	or die("Can't change directory to /tmp/temp\n");

defined($_ = $ftp->stat('.'))
	or print("not ok 4\n"), exit(1);
print("ok 4\n");

if (1) {
	print "length = ".length($_)."\n";
	for my $i (split(/\n/)) {
		print "+ $i\n";
	}
}

defined($_ = $ftp->list('.'))
	or print("not ok 5\n"), exit(1);
print("ok 5\n");

if (1) {
	print "length = ".length($_)."\n";
	for my $i (split(/\n/)) {
		print "+ $i\n";
	}
}

$ftp->quit
	or print("not ok 6\n"), exit(1);
print("ok 6\n");
