BEGIN {
	$| = 1; print "1..1\n";
}
END {
	print("not ok 1\n") unless $loaded;
}

use Fan::MD5;
use strict;
use vars qw($loaded %hash_data %hash_file $err);
$loaded = 1;
print "ok 1\n";

$Fan::MD5::LOG = 6;

# $str{n_0_9} = join('', '0'..'9');
# $str{n_a_z} = join('', 'a'..'z');
# $str{n_A_Z} = uc($str{n_a_z});

%hash_data = (
	''			=> 'd41d8cd98f00b204e9800998ecf8427e',
	'a'			=> '0cc175b9c0f1b6a831c399e269772661',
	'abc'			=> '900150983cd24fb0d6963f7d28e17f72',
	'0123456789'		=> '781e5e245d69b566979b86e28d23f2c7',
	'abcdefghijklmnopqrstuvwxyz',
				=> 'c3fcd3d76192e4007dfb496cca67e13b',
	'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
				=> 'd174ab98d277d9f5a5611c2c9f419d9f',
	'0123456789' x 5,	=> 'baed005300234f3d1503c50a48ce8e6f',
);

%hash_file = (
	'global.h'		=> '7af058e80a1ef5f247a36dc063476c46',
	'rfc1321.txt',		=> '754b9db19f79dbc4992f7166eb0f37ce',
);

# clear counter
$err = 0;

###
for my $key (keys %hash_data) {
	my $val = $hash_data{$key};
	my $ret = MD5String($key);
	print("try dat a: $key\n");
	if ($val eq $ret) {
		print("         : $ret ... o.k.\n");
	} else {
		print("         : $ret ... should be $val\n"), $err++;
	}
}
$err == 0
	or print("not ok 2\n"), exit(1);
print("ok 2\n");

###
for my $key (keys %hash_file) {
	my $val = $hash_file{$key};
	print("file $key not found\nnot ok 3\n"), exit(1) if ! -f $key;
	my $ret = MD5File($key);
	print("try file : $key\n");
	if ($val eq $ret) {
		print("         : $ret ... o.k.\n");
	} else {
		print("         : $ret ... should be $val\n"), $err++;
	}
}
$err == 0
	or print("not ok 3\n"), exit(1);
print("ok 3\n");
