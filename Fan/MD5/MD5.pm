;#
;# Copyright (c) 1995-1998
;#	Ikuo Nakagawa. All rights reserved.
;#
;# Redistribution and use in source and binary forms, with or without
;# modification, are permitted provided that the following conditions
;# are met:
;#
;# 1. Redistributions of source code must retain the above copyright
;#    notice unmodified, this list of conditions, and the following
;#    disclaimer.
;# 2. Redistributions in binary form must reproduce the above copyright
;#    notice, this list of conditions and the following disclaimer in the
;#    documentation and/or other materials provided with the distribution.
;#
;# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;# PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS
;# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
;# OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
;# OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
;# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;#
;# $Id: MD5.pm,v 1.13 1998/09/19 03:57:55 ikuo Exp $
;#
package Fan::MD5;

use strict;
use vars qw($VERSION @ISA @EXPORT);

require Exporter;
require DynaLoader;
use AutoLoader 'AUTOLOAD';

@ISA = qw(Exporter DynaLoader);
@EXPORT = qw(MD5Init MD5Update MD5Final MD5File MD5String);
$VERSION = '0.03';

;# proptotypes
;# sub MD5Init ();
;# sub MD5Update ($$;$);
;# sub MD5Final ($);
;# sub MD5File ($);
;# sub MD5String ($);

bootstrap Fan::MD5 $VERSION;

;#

;# A special marker for AutoSplit.
1;
__END__

sub MD5File ($) {
	my $file = shift;
	my $p = &MD5Init;
	my $length;
	local(*FILE, $_);

	open(FILE, $file) || return undef;
	while (($length = sysread(FILE, $_, 1024)) > 0) {
		&MD5Update($p, $_, $length);
	}
	close(FILE);
	my $str = &MD5Final($p);
	undef $p;
	$str;
}

sub MD5String ($) {
	my $string = shift;
	my $p = &MD5Init;
	&MD5Update($p, $string, length($string));
	my $str = &MD5Final($p);
	undef $p;
	$str;
}

;# end of Fan::MD5 module
