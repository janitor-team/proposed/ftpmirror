;#
;# Copyright (c) 1995-1997
;#	Ikuo Nakagawa. All rights reserved.
;#
;# Redistribution and use in source and binary forms, with or without
;# modification, are permitted provided that the following conditions
;# are met:
;#
;# 1. Redistributions of source code must retain the above copyright
;#    notice unmodified, this list of conditions, and the following
;#    disclaimer.
;# 2. Redistributions in binary form must reproduce the above copyright
;#    notice, this list of conditions and the following disclaimer in the
;#    documentation and/or other materials provided with the distribution.
;#
;# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
;# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
;# PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS
;# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
;# OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
;# OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
;# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
;# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;#
;# $Id: Usage.pm,v 1.8 1997/09/02 11:14:41 ikuo Exp $
;#
package Fan::Usage;

use strict;
use vars qw($VERSION @ISA @EXPORT $LOG);

require Exporter;
require DynaLoader;

@ISA = qw(Exporter DynaLoader);
@EXPORT = qw(getrusage);
$VERSION = '0.02';
$LOG = 5;

bootstrap Fan::Usage $VERSION;

sub UsagePtr::dump {
	my $p = shift;

	printf("%10.4f user time used\n", $p->ru_utime);
	printf("%10.4f system time used\n", $p->ru_stime);
	printf("%10d maximum resident set size\n", $p->ru_maxrss);
	printf("%10d integral shared memory size\n", $p->ru_ixrss);
	printf("%10d integral unshared data size\n", $p->ru_idrss);
	printf("%10d integral unshared stack size\n", $p->ru_isrss);
	printf("%10d page reclaims\n", $p->ru_minflt);
	printf("%10d page faults\n", $p->ru_majflt);
	printf("%10d swaps\n", $p->ru_nswap);
	printf("%10d block input operations\n", $p->ru_inblock);
	printf("%10d block output operations\n", $p->ru_oublock);
	printf("%10d messages sent\n", $p->ru_msgsnd);
	printf("%10d messages received\n", $p->ru_msgrcv);
	printf("%10d signals received\n", $p->ru_nsignals);
	printf("%10d voluntary context switches\n", $p->ru_nvcsw);
	printf("%10d involuntary context switches\n", $p->ru_nivcsw);
}

;# A special marker for AutoSplit.
1;
__END__

;# end of Fan::Usage module
