BEGIN {
	$| = 1; print "1..1\n";
}
END {
	print("not ok 1\n") unless $loaded;
}

$Fan::Usage::LOG = 6;
use Fan::Usage;
use strict;
use vars qw($loaded);
$loaded = 1;
print("ok 1\n");

{
	my $usage = getrusage(0);
	for (my $i = 0; $i < 10000; $i++) {
		local *FILE;
		open(FILE, "/dev/null") && close(FILE);
	}
	print "$usage\n";
	print "utime = ".$usage->ru_utime."\n";
	print "stime = ".$usage->ru_stime."\n";
	print "maxrss = ".$usage->ru_maxrss."\n";
	print "ixrss = ".$usage->ru_ixrss."\n";
	print "idrss = ".$usage->ru_idrss."\n";
	print "isrss = ".$usage->ru_isrss."\n";
	print "minflt = ".$usage->ru_minflt."\n";
	print "majflt = ".$usage->ru_majflt."\n";
	print "nswap = ".$usage->ru_nswap."\n";
	print "inblock = ".$usage->ru_inblock."\n";
	print "oublock = ".$usage->ru_oublock."\n";
	print "msgsnd = ".$usage->ru_msgsnd."\n";
	print "msgrcv = ".$usage->ru_msgrcv."\n";
	print "nsignals = ".$usage->ru_nsignals."\n";
	print "nvcsw = ".$usage->ru_nvcsw."\n";
	print "nivcsw = ".$usage->ru_nivcsw."\n";
	undef $usage;
}
print("ok 2\n");

{
	my $usage = getrusage;
	$usage->dump;
	print "clock tick = ".$usage->clk_tck."\n";
}
print("ok 3\n");
#sleep(5);
