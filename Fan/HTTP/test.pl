BEGIN {
	$| = 1;
	print("1..3\n");
}
END {
	print("not ok 1\n") unless $loaded;
}

sub prompt {
	my $prompt = shift;
	local $_;

	print STDERR $prompt;
	defined($_ = <STDIN>) || return undef;
	chomp;
	$_;
}

use Fan::HTTP;
$loaded = 1;
print("ok 1\n");

$proxy = &prompt("proxy: ");
$url = &prompt("url: ");

ref($http = Fan::HTTP->new(http_proxy => $proxy))
	or print("not ok 2\n"), exit(1);
print("ok 2\n");

$http->get($url, "tmp.out")
	or print("not ok 3\n"), exit(1);
print("ok 3\n");

print("check tmp.out to see contents of $url.\n");
