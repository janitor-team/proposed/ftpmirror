BEGIN { $| = 1; print "1..4\n"; }
END { print "not ok 1\n" unless $loaded; }

use Fan::Param;
$loaded = 1;
print("ok 1\n");

$Fan::Param::LOG = 7;

sub proc {
	local $_ = shift;

	/proc/i ? $_ : undef;
}

%pkeys = (
	key_any		=> '',
	key_int		=> 'INTEGER',
	key_bool	=> 'BOOLEAN',
	key_code	=> 'CODE',
	key_hash	=> 'HASH',
	key_proc	=> \&proc,
);

$param = Fan::Param->new( param_name => 'TEST', param_keys => \%pkeys );
ref($param) && $param->isa('Fan::Param')
	or print("not ok 2\n"), exit;
print("ok 2\n");

$success = eval { $param->setval('key_any', 'Anything is o.k.'), 'key_any' }
	&& eval { $param->setval('key_proc', 'PROCEDURE'), 'key_proc' }
	&& eval { $param->setval('key_hash', \%pkeys), 'key_hash' }
	&& eval { $param->setval('key_code', \&proc), 'key_code' }
	&& eval { $param->setval('key_bool', 'FALSE'), 'key_bool' };
$success or print("not ok 3: $@"), exit;
print("ok 3\n");

$failure = eval { $param->getval('not_a_key'), 'not_a_key' }
	|| eval { $param->setval('key_int', 'no int'), 'key_proc' }
	|| eval { $param->setval('key_proc', \%pkeys), 'key_proc' }
	|| eval { $param->setval('key_hash', \&proc), 'key_hash' }
	|| eval { $param->setval('key_code', 20), 'key_code' }
	|| eval { $param->setval('key_bool', "must"), 'key_code' };
$failure and print("not ok 4 ($failure): $@\n"), exit;
print("ok 4\n");
